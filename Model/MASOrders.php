<?php

namespace MASConnect\MASConnectModule\Model;

use Magento\Framework\Model\AbstractModel;

class MASOrders extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('MASConnect\MASConnectModule\Model\Resource\MASOrders');
    }
}