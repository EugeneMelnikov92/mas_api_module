<?php

namespace MASConnect\MASConnectModule\Model\Resource\MASOrders;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'MASConnect\MASConnectModule\Model\MASOrders',
            'MASConnect\MASConnectModule\Model\Resource\MASOrders'
        );
    }
}