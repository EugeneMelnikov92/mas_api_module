<?php

namespace MASConnect\MASConnectModule\Model\Resource;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class MASOrders extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('mas_orders', 'id');
    }
}