<?php

namespace MASConnect\MASConnectModule\Model\Ui\Component\Listing\Column\CcType;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory;

class CcType implements OptionSourceInterface
{

    protected $salesCollection;

    protected $options;

    public function __construct(
        \Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory $collectionFactory
    ) {
        $this->salesCollection = $collectionFactory->getReport("sales_order_grid_data_source");
    }

    public function toOptionArray()
    {
        if ($this->options === null) {

            foreach ($this->salesCollection->addOrder('cc_type')->addFieldToSelect('cc_type')->distinct(true) as $order) {
                $ccType          = $order->getCcType();
                $this->options[] = [
                    'value' => $ccType,
                    'label' => $ccType
                ];
            }
        }

        return $this->options;
    }

}