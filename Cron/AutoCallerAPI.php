<?php

namespace MASConnect\MASConnectModule\Cron;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\App\ObjectManager;

use MASConnect\MASConnectModule\API\Endpoints\ConfirmanOrder;
use MASConnect\MASConnectModule\API\Endpoints\GetOrdersForAFlorist;

class AutoCallerAPI
{
    protected $orderRepository;
    protected $scopeConfig;
    protected $storeManager;

    protected $objectManager;

    public function __construct(
        OrderRepositoryInterface $OrderRepositoryInterface
    ) {
        $this->objectManager = ObjectManager::getInstance();

        $this->storeManager = $this->objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $this->scopeConfig  = $this->objectManager->get('\Magento\Framework\App\Helper\Context')->getScopeConfig();

        $this->orderRepository = $OrderRepositoryInterface;
    }

    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/masapilogger.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $OrderData = $this->objectManager->create('MASConnect\MASConnectModule\Model\MASOrders');
        $allOrders = $OrderData->getCollection();

        foreach ($allOrders as $order) {

            $logger->info($order->getMessageNumber() . ' - message number! ');

            $messageNumber = $order->getMessageNumber();
            $orderId       = $order->getNotConfirmedOrder();

            try {

                $OrderConfirm = (new ConfirmanOrder(
                    $this->scopeConfig,
                    $this->storeManager,
                    $messageNumber
                ));
                $response     = $OrderConfirm->get();

//                $logger->info($orderId . ' - order id! (try)');

                $OrderData->load($orderId, 'not_confirmed_order')->delete();
                $salesOrder   = $this->objectManager->create('\Magento\Sales\Model\Order');
                $currentOrder = $salesOrder->load($orderId, 'increment_id');
                $currentOrder->setMasApiStatus('Confirmed');
                $currentOrder->save();

            } catch (\Exception $exception) {
                $OrderData->load($orderId, 'not_confirmed_order')->delete();
            }

        }

//        $orderDatamodel = $this->objectManager->get('\Magento\Sales\Model\Order')->getCollection();
//
//        foreach ($orderDatamodel as $testOrder) {
//            $logger->info($testOrder->getMasApiStatus() . ' - mas api status! ');
//        }

    }
}