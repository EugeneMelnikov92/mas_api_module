<?php

namespace MASConnect\MASConnectModule\API\Endpoints;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use MASConnect\MASConnectModule\API\APIEndpoints;

class GetOrdersForAFlorist extends APIEndpoints
{
    /**
     * GetOrdersForAFlorist constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param array $pathArgs
     */
    public function __construct(ScopeConfigInterface $scopeConfig, StoreManagerInterface $storeManager, ...$pathArgs)
    {
        parent::__construct($scopeConfig, $storeManager);

        $this->path     = 'orders?messagenumber=%d';
        $this->pathArgs = $pathArgs;
    }
}