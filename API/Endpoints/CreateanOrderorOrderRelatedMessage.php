<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 26.09.18
 * Time: 9:54
 */

namespace MASConnect\MASConnectModule\API\Endpoints;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use MASConnect\MASConnectModule\API\APIEndpoints;

class CreateanOrderorOrderRelatedMessage extends APIEndpoints
{
    public function __construct(ScopeConfigInterface $scopeConfig, StoreManagerInterface $storeManager, ...$pathArgs)
    {
        parent::__construct($scopeConfig, $storeManager);

        $this->path     = 'messagesj';
        $this->pathArgs = $pathArgs;
    }
}