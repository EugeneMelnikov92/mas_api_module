<?php

namespace MASConnect\MASConnectModule\API\Endpoints;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use MASConnect\MASConnectModule\API\APIEndpoints;

class ConfirmanOrder extends APIEndpoints
{

    public function __construct(ScopeConfigInterface $scopeConfig, StoreManagerInterface $storeManager, ...$pathArgs)
    {
        parent::__construct($scopeConfig, $storeManager);

        $this->path     = 'confirmorder?messagenumber=%d';
        $this->pathArgs = $pathArgs;
    }
}