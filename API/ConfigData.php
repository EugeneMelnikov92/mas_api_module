<?php

namespace MASConnect\MASConnectModule\API;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

abstract class ConfigData
{
    /**
     * Core store config
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */

    protected $scopeConfig;
    protected $storeManager;

    protected $ModuleStatus;
    protected $username;
    protected $password;
    protected $masdirectid;
    protected $APIURL;
    protected $NotEmptyData;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig  = $scopeConfig;
        $this->storeManager = $storeManager;

        $this->username    = $this->scopeConfig->getValue(
            'MASConnect/general/username',
            ScopeInterface::SCOPE_STORE
        );
        $this->password    = $this->scopeConfig->getValue(
            'MASConnect/general/password',
            ScopeInterface::SCOPE_STORE
        );
        $this->masdirectid = $this->scopeConfig->getValue(
            'MASConnect/general/masdirectid',
            ScopeInterface::SCOPE_STORE
        );

        $this->APIURL = $this->scopeConfig->getValue(
            'MASConnect/general/api_url',
            ScopeInterface::SCOPE_STORE
        );

        $this->ModuleStatus = $this->scopeConfig->getValue(
            'MASConnect/general/enable',
            ScopeInterface::SCOPE_STORE
        );

        $this->NotEmptyData = $this->isDataFilled();
    }

    /**
     * Check is not empty data
     * @return bool
     */

    public function isDataFilled()
    {
        return ! empty($this->masdirectid) && (($this->ModuleStatus) != false);
    }

    /**
     * @return mixed
     */
    public function getAPIURL()
    {
        return $this->APIURL;
    }

    /**
     * @param mixed $APIURL
     */
    public function setAPIURL($APIURL)
    {
        $this->APIURL = $APIURL;
    }

    /**
     * @return mixed
     */
    public function getMasdirectid()
    {
        return $this->masdirectid;
    }

    /**
     * @param mixed $masdirectid
     */
    public function setMasdirectid($masdirectid)
    {
        $this->masdirectid = $masdirectid;
    }

    /**
     * @return mixed
     */
    public function getModuleStatus()
    {
        return $this->ModuleStatus;
    }

    /**
     * @param mixed $ModuleStatus
     */
    public function setModuleStatus($ModuleStatus)
    {
        $this->ModuleStatus = $ModuleStatus;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return ScopeConfigInterface
     */
    public function getScopeConfig()
    {
        return $this->scopeConfig;
    }

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function setScopeConfig($scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return StoreManagerInterface
     */
    public function getStoreManager()
    {
        return $this->storeManager;
    }

    /**
     * @param StoreManagerInterface $storeManager
     */
    public function setStoreManager($storeManager)
    {
        $this->storeManager = $storeManager;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return bool
     */
    public function isNotEmptyData()
    {
        return $this->NotEmptyData;
    }

    /**
     * @param bool $NotEmptyData
     */
    public function setNotEmptyData($NotEmptyData)
    {
        $this->NotEmptyData = $NotEmptyData;
    }

}