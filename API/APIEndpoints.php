<?php

namespace MASConnect\MASConnectModule\API;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use phpDocumentor\Reflection\Types\Array_;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;

use MASConnect\MASConnectModule\API\ConfigData;
use MASConnect\MASConnectModule\Observer;

class APIEndpoints extends ConfigData
{

    public $requestData = array();
    protected $scopeConfig;
    protected $storeManager;
    protected $pathArgs;
    protected $authheader;
    protected $baseUri;
    protected $path;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($scopeConfig, $storeManager);
        $this->authheader = base64_encode(utf8_encode($this->username . ':' . $this->password . ':' . $this->masdirectid));
    }

    /**
     * @param array $pathArgs
     */
    public function setPathArgs(
        ...$pathArgs
    ) {
        $this->pathArgs = $pathArgs;
    }

    public function getauthheader()
    {
        return $this->authheader;
    }

    /**
     * @param string $endpointPath
     *
     * @return string
     */
    public function pathMaker(
        $endpointPath = null
    ) {
        if ( ! is_null($endpointPath)) {
            return sprintf($endpointPath, ...$this->pathArgs);
        } else {
            return sprintf($this->path, ...$this->pathArgs);
        }
    }

    public function get()
    {
        if ($this->NotEmptyData) {
            try {
                $response = null;

                $client = new Client([
                    'base_uri' => $this->APIURL,
                    'timeout'  => 3.0,
                ]);

                try {
                    $request = new Request('GET', $this->pathMaker(), [
                        'Authorization' => 'Basic ' . $this->authheader,
                        'ContentType'   => 'application/json; charset=utf-8'
                    ]);

                    $response = $client->send($request);
                    $response = $response->getBody();

                    return $response;

                } catch (\Exception $exception) {
//                TODO: error handler
                }

//                return $response;

            } catch (GuzzleException $exception) {
//                TODO: error handler
            }
        }
    }

    public function post($data)
    {
        if ($this->NotEmptyData) {
            try {
                $responce = null;

                $client = new Client([
                    'base_uri' => $this->APIURL,
                    'timeout'  => 5.0
                ]);

                try {
                    $request = $client->post($this->pathMaker(),
                        array(
                            'headers' => [
                                'Authorization' => 'Basic ' . $this->authheader,
                                'ContentType'   => 'application/json; charset=utf-8'
                            ],
                            'json'    => $data
                        ));

                    $response = $request->getBody()->getContents();

                    return $response;

                } catch (\Exception $exception) {
//                TODO: error handler
                }

            } catch (\Exception $exception) {
//                TODO: error handler
            }
        }
    }
}