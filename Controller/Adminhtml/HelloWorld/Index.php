<?php

namespace MASConnect\MASConnectModule\Controller\Adminhtml\HelloWorld;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Result\PageFactory;
use MASConnect\MASConnectModule\API\APIEndpoints;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use MASConnect\MASConnectModule\API\Endpoints\CreateanOrderorOrderRelatedMessage;
use MASConnect\MASConnectModule\API\Endpoints\GetOrdersForAFlorist;
use MASConnect\MASConnectModule\API\Endpoints\ConfirmanOrder;
use MASConnect\MASConnectModule\Observer\ChangeDisplayText;
use Magento\Sales\Api\OrderRepositoryInterface;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    protected $context;
    protected $storeManager;
    protected $scopeConfig;
    protected $orderRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        OrderRepositoryInterface $OrderRepositoryInterface
    ) {
        parent::__construct($context);
        $this->context           = $context;
        $this->resultPageFactory = $resultPageFactory;

        $objectManager = ObjectManager::getInstance();

        $this->storeManager    = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $this->scopeConfig     = $objectManager->get('\Magento\Framework\App\Helper\Context')->getScopeConfig();
        $this->orderRepository = $OrderRepositoryInterface;
    }

    /**
     * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
//        $apiEndpoints = (new GetOrdersForAFlorist(
//            $this->scopeConfig,
//            $this->storeManager,
//            89342
//        ))->get();

        $OrderConfirm = (new ConfirmanOrder(
            $this->scopeConfig,
            $this->storeManager,
            125853
        ))->get();

        echo '<pre>';
//        $Caller = new APIEndpoints(
//            $this->scopeConfig,
//            $this->storeManager
//        );
//        var_dump($Caller->getMasdirectid());
//        var_dump($Caller->getUsername());
//        var_dump($Caller->getPassword());
//        var_dump($Caller->getAPIURL());
//        var_dump($Caller->isNotEmptyData());
//        var_dump($Caller->isDataFilled());
//        var_dump($Caller->getauthheader());
        echo '</pre>';
        die();

        return $resultPage = $this->resultPageFactory->create();
    }
}

?>