# MAS API Module

### Installation

For install this module you need to move module directory to `<project name>/app/code` and run several commands from the project root directory:

```sh
$ php bin/magento setup:upgrade
$ php bin/magento cache:flush
$ php bin/magento cron:install
$ php bin/magento setup:di:compile
```
and several commands from the module root directory:

```sh
$ composer install
```

then you need to configure cron on your server and reload cron 

```
* * * * * <path to php binary> <magento install dir>/bin/magento cron:run | grep -v "Ran jobs by schedule" >> <magento install dir>/var/log/magento.cron.log
* * * * * <path to php binary> <magento install dir>/update/cron.php >> <magento install dir>/var/log/update.cron.log
* * * * * <path to php binary> <magento install dir>/bin/magento setup:cron:run >> <magento install dir>/var/log/setup.cron.log</pre>
```

where

`<path to php binary>` is the absolute file system path to your PHP binary
`<magento install dir>` is the directory in which you installed the Magento software; for example, /var/www
`| grep -v "Ran jobs by schedule"` filters this message from the log, making any errors easier to spot

and then run 
`
$ sudo service cron reload
`

[For more information read magento 2 documentations](https://devdocs.magento.com/guides/v2.2/config-guide/cli/config-cli-subcommands-cron.html)

### Business logic
This module originates in the class `ConfigData`, which is responsible for the necessary data for working with the API. 
This class get from `config_data` table a  fields with data from settings page this module.
***
Then `APIEndpoints` class implement GET and POST methods and required URL without params.
Also, Endpoint-classes are also inherited from `APIEndpoints`. i.e., for each endpoint in the API this module has its own class, which is inherited from an `APIEndpoints` with its own parameters.

######  Example:

| Endpoint-class        | Method           
| ------------- |:-------------:
| CreateanOrderorOrderRelatedMessage      | POST 
| ConfirmanOrder      | GET      
| GetOrdersForAFlorist | GET      
***
`MasApiCaller` class is an observer of Magento events. At each successful purchase, he selects the data of the order from magenta and builds the required json.

Then this observer call POST method via `CreateanOrderorOrderRelatedMessage` class and save response from API in new data table `mas_orders`

######  Example:

| Id        | Not Confirmed Order   | Message Number        | Control Number  | Message        
| ------------- |:-------------:| ------------- |:-------------:|:-------------:
| 1| 000000286| 125961| 12149 | Verified. Order 12149 has been...  
| 2| 000000287| 125962| 12150 | Verified. Order 12150 has been...  

And, also this class set default value Unknown for `mas_api_status` in the orders table.  

***

Then `AutoCallerAPI`, which is an CRON task manager, every minute send GET request to `ConfirmanOrder` endpoint and check order status in the API. 

If the order status is **confirmed**, this order is deleted from the data table of `mas_orders` and the status for this order in the table with all orders is changed to `confirmed`.

If the order status is **not confirmed**, CRON will send this request every minute until it receives a successful result.














