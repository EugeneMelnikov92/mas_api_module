<?php

namespace MASConnect\MASConnectModule\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $table = $installer->getConnection()
                           ->newTable($installer->getTable('mas_orders'))
                           ->addColumn(
                               'id',
                               \Magento\Framework\Db\Ddl\Table::TYPE_INTEGER,
                               null,
                               ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
                               'Id'
                           )->addColumn(
                'not_confirmed_order',
                \Magento\Framework\Db\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Not Confirmed Order'
            )->addColumn(
                'message_number',
                \Magento\Framework\Db\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Message Number'
            )->addColumn(
                'control_number',
                \Magento\Framework\Db\Ddl\Table::TYPE_INTEGER,
                null,
                [],
                'Control Number'
            )->addColumn(
                'message',
                \Magento\Framework\Db\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => \Magento\Framework\Db\Ddl\Table::TIMESTAMP_INIT],
                'Message'
            );
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
