<?php

namespace MASConnect\MASConnectModule\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\App\ObjectManager;

use MASConnect\MASConnectModule\API\Endpoints\CreateanOrderorOrderRelatedMessage;

class MasApiCaller implements ObserverInterface
{
    protected $orderRepository;
    protected $scopeConfig;
    protected $storeManager;

    protected $objectManager;

    public function __construct(
        OrderRepositoryInterface $OrderRepositoryInterface
    ) {
        $this->objectManager = ObjectManager::getInstance();

        $this->storeManager = $this->objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $this->scopeConfig  = $this->objectManager->get('\Magento\Framework\App\Helper\Context')->getScopeConfig();

        $this->orderRepository = $OrderRepositoryInterface;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        // Preparing data for API
        $order_ids      = $observer->getEvent()->getOrderIds()[0];
        $order          = $this->orderRepository->get($order_ids);
        $order_id       = $order->getIncrementId();
        $customer_email = $order->getCustomerEmail();

        $customerId = $order->getCustomerId();
        $StoreName  = $order->getStoreName();
        $OrderInfo  = $observer->getData();
        $info       = $observer->getEvent()->getData();
        $shipping   = $order->getBillingAddress();
        $payment    = $order->getPayment();

        $RecipientAddress = $shipping->getStreet();
        $RecipientAddress = implode(' ', $RecipientAddress);

        $SendData = array(
            "MessageType"       => '0',
            "MessageText"       => 'Here is some text for this message.',
            'SenderMdNumber'    => 'USZZ000033',
            'FulfillerMDNumber' => 'USTX000001',
            "OrderItems"        => [

            ],
            "RecipientDetail"   => [
                'RecipientFirstName'  => $shipping->getFirstname(),
                'RecipientLastName'   => $shipping->getLastname(),
                'ResidenceType'       => '1',
                'RecipientAttention'  => 'Attention',
                'RecipientAddress'    => $RecipientAddress,
                'RecipientCity'       => $shipping->getCity(),
                'RecipientState'      => $shipping->getRegion(),
                'RecipientCountry'    => $shipping->getCountryId(),
                'RecipientZip'        => $shipping->getPostcode(),
                'DeliveryDate'        => '12/11/2018',
                'DeliveryEndDate'     => '12/11/2018',
                "SpecialInstructions" => "Leave on front porch if no answer",
                "OccasionType"        => "1"
            ]
        );

        foreach ($order->getAllVisibleItems() as $item) {
            $OrderInfo = $item->getData();

            array_push($SendData['OrderItems'], array(
                'ItemCode'        => $OrderInfo["item_id"],
                'ItemName'        => $OrderInfo["name"],
                'ItemDescription' => $OrderInfo["sku"],
                'ItemCost'        => preg_replace("/(?<=\\.[0-9]{2})[0]+\$/", "", $OrderInfo["price"]),
                'Quantity'        => strval($OrderInfo["product_options"]["info_buyRequest"]["qty"])
            ));
        }

        // Call POST request by checkout page event
        $OrderCreate = new CreateanOrderorOrderRelatedMessage(
            $this->scopeConfig,
            $this->storeManager
        );
        $response    = $OrderCreate->post($SendData);
        $response    = json_decode($response, true);

        // Save response from API  on the POST request
        $SavedData = $this->objectManager->create('MASConnect\MASConnectModule\Model\MASOrders');
        $SavedData->setNotConfirmedOrder($order_id);
        $SavedData->setMessageNumber($response['Messages']['MessageNumber']);
        $SavedData->setControlNumber($response['Messages']['ControlNumber']);
        $SavedData->setMessage($response['Messages']['Message']);
        $SavedData->save();

        // Default value of the confirmation status
        $salesOrder   = $this->objectManager->create('\Magento\Sales\Model\Order');
        $currentOrder = $salesOrder->load($order_id, 'increment_id');
        $currentOrder->setMasApiStatus('Unknown');
        $currentOrder->save();
    }
}